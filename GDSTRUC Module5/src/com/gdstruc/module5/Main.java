package com.gdstruc.module5;

public class Main {

    public static void main(String[] args) {
        Player kirito =new Player(1,"Kirito", 450);
        Player slayers =new Player(2,"Slayers", 300);
        Player douchebag =new Player(1,"DoucheBag", 250);
        Player night =new Player(1,"Night", 270);
        Player asuna =new Player(1,"Asuna", 270);

       SimpleHashTable hashTable=new SimpleHashTable();
       hashTable.put(kirito.getUsername(), kirito);
       hashTable.put(slayers.getUsername(), slayers);
       hashTable.put(douchebag.getUsername(), douchebag);
       hashTable.put(night.getUsername(), night);
       hashTable.put(asuna.getUsername(), asuna);

       hashTable.printHashtable();
       hashTable.remove("Kirito");

       hashTable.printHashtable();
    }
}
