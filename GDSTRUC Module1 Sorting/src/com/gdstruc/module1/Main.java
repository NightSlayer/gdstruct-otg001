package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {
	   int[] numbers= new int [10];
	    numbers[0]=41;
        numbers[1]=63;
        numbers[2]=4;
        numbers[3]=98;
        numbers[4]=69;
        numbers[5]=32;
        numbers[6]=111;
        numbers[7]=21;
        numbers[8]=10;
        numbers[9]=55;
        printArrayElements(numbers);
        SelectionSort(numbers);
        System.out.println("Bubble Sort");
        printArrayElements(numbers);
    }
    private static void BubbleSort(int[]arr){
        for(int lastSortedIndex= arr.length-1; lastSortedIndex>0; lastSortedIndex--){
            for(int i= 0; i<= lastSortedIndex; i++){
                if(arr[i]< arr [i+1]){
                      int temp=arr[i];
                      arr[i]=arr[i+1];
                      arr[i+1]=temp;
                }
            }
        }
    }
    private static void SelectionSort(int[] arr){
        int LargestIndex=0;
        for(int lastSortedIndex= arr.length-1; lastSortedIndex>0; lastSortedIndex--){
            for(int i= 0; i<= lastSortedIndex; i++){
                if(arr[i]< arr[LargestIndex]){
                    LargestIndex=i;
                }
            }
            int temp=arr[lastSortedIndex];
            arr[lastSortedIndex]=arr[LargestIndex];
            arr[LargestIndex]=temp;
        }
    }
 
    private static void printArrayElements(int[]arr){
        for (int j: arr){
            System.out.println(j);
        }
    }
}
