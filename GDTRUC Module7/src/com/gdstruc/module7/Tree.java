package com.gdstruc.module7;

public class Tree {
    private Node root;

    public void insert(int value){
        if(root==null){
            root=new Node(value);
        }
        else{
            root.insert(value);
        }
    }
    public void traverseInOrder(){
        if(root!=null){
            root.traverseInOrder();
        }
    }
    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }
    public Node GetMin(){
        if(root!=null){
            return root.GetMin();
        }
        return null;
    }
    public Node GetMax(){
        if(root!=null){
            return root.GetMax();
        }
        return null;
    }
    public void traverseInOrderDescending(){
        if(root!=null){
            root.traverseInOrderDescending();
        }
    }
}
