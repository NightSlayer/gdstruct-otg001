package com.gdstruc.module2;

public class PlayerLinkedList {
    private PlayerNode head;

    public void addToFront(Player player){
        PlayerNode playerNode= new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList(){
        PlayerNode current= head;
        int size=0;
        int index=0;
        System.out.print("Head-> ");
        while(current!= null){
            size++;
            System.out.print(current);
            System.out.print("->");
            current=current.getNextPlayer();
            index++;
        }
        System.out.print("NULL");
        System.out.print("Size: ");
        System.out.print(size);
    }

    public void removeFront(){
        PlayerNode current= head;
        PlayerNode temp=null;
        temp=current;
        current=current.getNextPlayer();
        head = current;
    }

    public void contain(Player player, String words){
        PlayerNode playerNode= new PlayerNode(player);
        System.out.println(playerNode.contains(words));
    }




}
