package com.gdstruc.module2;

public class Main {

    public static void main(String[] args) {
	    Player kirito =new Player(1,"Kirito", 450);
        Player slayer =new Player(2,"Slayer", 300);
        Player douche =new Player(1,"Douche", 250);

        PlayerLinkedList playerLinkedList =new PlayerLinkedList();

        playerLinkedList.addToFront(kirito);
        playerLinkedList.addToFront(slayer);
        playerLinkedList.addToFront(douche);

        playerLinkedList.printList();

        playerLinkedList.removeFront();

        playerLinkedList.printList();
    }
}
